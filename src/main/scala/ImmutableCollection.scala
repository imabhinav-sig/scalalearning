import scala.collection.BitSet
import scala.collection.immutable.{ListMap, ListSet, Queue, TreeMap, Stream}

object ImmutableCollection extends App {
  /**
   * List
   */
  val list1: List[String] = List("qwerty", "asdf")
  println(list1)
  println(list1(1))

  val list2 = list1 :+ "abcd" // Appending to last
  println(list2)

  val list3 = "zxcv" +: list1 // Appending to front
  println(list3)

  val list4 = list1 :: list2 // Add two list List(List(qwerty, asdf), qwerty, asdf, abcd)
  println(list4)

  val list5 =  list1 ::: list2
  println(list5) // List(qwerty, asdf, qwerty, asdf, abcd)

  val emptyList: List[String] = List.empty[String] // Empty List
  println(emptyList)

  println("\n")
  /**
   * ListSet
   */
  val listSet1: ListSet[String] = ListSet("qwerty", "asdf")
  println(listSet1)
  println(listSet1("qwerty"))

  val listSet2: ListSet[String] = listSet1 + "zxcv"
  println(listSet2)

  val listSet3 = listSet1 ++ ListSet("abhi")
  println(listSet3)

  val listSet4 = listSet3 - "abhi"
  println(listSet4)

  val listSet5 = ListSet.empty[String]
  println("\n")

  /**
   * ListMap
   */
  val listMap1: ListMap[String, String] = ListMap("PD" -> "Plain Donut", "SD" ->"Strawberry Donut", "CD" -> "Chocolate Donut")
  println(listMap1)
  println(listMap1("CD"))

  val listMap2 = listMap1 + ("KD" -> "Krispy Donut")
  println(listMap2)

  val listMap3 = listMap1 ++ listMap2
  println(listMap3)

  val listMap4 = listMap3 - "KD"
  println(listMap4)

  val listMap5 = ListMap.empty[String, String]
  println(listMap5)

  /**
   * Map
   */
  //Initialised using tuples
  val map1: Map[String, String] = Map(("a", "A"),("b", "B"))
  println(map1)

  //Initialize Map using key -> value notation
  val map2: Map[String, String] = Map("a"->"A","b"->"B")
  println(map2)
  println(map2("b"))
  println()
  //rest operations same as ListMap

  /**
   * HashMap has same operations as Map
   */

  /**
   * TreeMap
   */
  val treeMap1: TreeMap[String, String] = TreeMap("b"->"B", "a"->"A")
  println(treeMap1) // Sorted Output
  println(treeMap1("b"))

  // + to add element
  // ++ to add two TreeMaps
  // - to remove element

  //How to change ordering of TreeMap to descending alphabet
  object newOrdering extends Ordering[String] {
    override def compare(x: String, y: String): Int = y.compareTo(x)
  }
  val treeMap2: TreeMap[String, String] = TreeMap("b"->"B", "a"->"A")(newOrdering)
  println(treeMap2)
  println()
  // .empty for empty TreeMap

  /**
   * Queue
   */
  val queue1: Queue[String] = Queue("a", "b", "c")
  println(queue1)
  println(queue1(2))

  val queue2 = queue1 :+ "d"
  println(queue2)

  val enqueue = queue1.enqueue("d")
  println(enqueue)

  //Calling dequeue returns a Tuple2 with the first element that was inserted to the Queue and the remaining elements of the queue
  val dequeue: (String, Queue[String]) = queue1.dequeue
  println(dequeue)
  println(dequeue._2)

  val queue3 = queue1 ++ queue2
  println(queue3)
  println()

  /**
   * Sequence => Same operations as list
   * Sequence is a trait, the elements of the Sequence are rendered into a concrete Immutable List
   */

  /**
   * Set
   */
  // Insertion, Deletion and Addition is same as LinkedSet

  val set1: Set[String] = Set("a","b","c")
  val set2: Set[String] = Set("c", "d", "e")
  //Intersection between two sets
  println(set1 & set2)
  //Difference between two sets
  println(set1 &~ set2)
  println()

  /**
   * HashSet
   */
  //The HashSet will use the element's hashCode as a key to allow for fast lookup of the element's value within the HashSet
  //The internal data structure of the HashSet in Scala is a HashTrie
  //Same operations as set

  /**
   * TreeSet
   */
  //TreeSet allows you to store unique elements but also provides ordering of the elements
  //TreeSet internally uses a Red-Back data structure to ensure a balanced tree that will provide O(log n) for most operations
  //Same operations as Set and Ordering can be changed

  /**
   * SortedTest
   */
  //SortedSet allows you to store unique elements but also provides ordering of the elements.
  //SortedSet is a trait. Its concrete class implementation is a TreeSet

  /**
   * BitSet
   */
  //BitSet also allows you to store unique elements with no repeatable values.
  //BitSet represents a collection of small integers as the bits of a larger integer
  val bitSet1: BitSet = BitSet(3,2,0)
  println(bitSet1)
  println(bitSet1(2))

  val bitSet2 = bitSet1 + 4
  println(bitSet2)
  println()

  // add two BitSet using ++
  // Remove using -
  // Intersection &, Difference &~

  /**
   * Stream (LazyList is fully lazy, it has only lazy tail
   */
  //As per the Scala Documentation, a Stream is a similar data structure to a list except that the elements of the Stream will be lazily computer
  val stream1: Stream[Int] = 1 #:: 2 #:: 3 #:: Stream.empty
  //alternate way
  //val stream1: Stream[Int] = cons(1, cons(2, cons(3, Stream.empty) ) )
  println(stream1)

  val lazyList1: LazyList[Int] = 1 #:: 2 #:: 3 #:: LazyList.empty
  println(lazyList1)

  stream1.take(3).print()
  println()// Print first 3 numbers
  println(lazyList1.take(3).mkString(" ")) // Print first 10 elements

  //InfiniteStream
  def infiniteNumberStream(number: Int): Stream[Int] = Stream.cons(number, infiniteNumberStream(number+1))
  infiniteNumberStream(1).take(20).print()

  val stream2: Stream[Int] = Stream.from(1)
  stream2.take(10).print()
  println()

  val lazyList2: LazyList[Int] = LazyList.from(3)
  println(lazyList2.take(20).mkString(" "))
}
