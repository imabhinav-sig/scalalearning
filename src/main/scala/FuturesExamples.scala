//We've had to import scala.concurrent.Future to have access to the Future type
import java.util.concurrent.Executors

import scala.concurrent.{Await, ExecutionContextExecutor, Future, Promise}
import scala.util.{Failure, Success, Try}
//We've also had to import scala.concurrent.ExecutionContext.Implicits.global which will place a default thread pool in scope on which our Future will be executed asynchronously.
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object FuturesExamples extends App {
//  val executor = Executors.newSingleThreadExecutor()
//  implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.fromExecutor(executor)

  def donutStock(donut: String): Future[Int] = Future {
    println("checking donut stock")
    10
  }

  /**
   * Blocking Future Result
   */
  //The donutStock() method will run asynchronously and for the purpose of this example are using Await.result() to block our main program and wait for the result from the donutStock() method
  val vanillaDonutStock = Await.result(donutStock("Vanilla Donut"), 5 seconds)
  println()

  donutStock("vanilla donut").onComplete{
    case Success(stock) => println(s"Stock : $stock")
    case Failure(e) => println("Failed to find stock")
  }
  Thread.sleep(3000)
  println()

  /**
   * Chain Futures using flatMap
   */
  def buyDonuts(qty: Int): Future[Boolean] = Future {
    println(s"buying $qty donuts")
    true
  }

  val buyingDonuts: Future[Boolean] = donutStock("plain donut").flatMap(qty => buyDonuts(qty))
  val isSuccess = Await.result(buyingDonuts, 5 seconds)
  println()

  /**
   * Future Sequence
   *  the sequence function is useful when you have to reduce a number of futures into a single future. Moreover,
   *  these futures will be non-blocking and run in parallel which also imply that the order of the futures is not guaranteed as they can be interleaved
   */
  def donutStock1(donut: String): Future[Option[Int]] = Future {
    println("checking donut stock.....sleep for 2 seconds")
    Thread.sleep(2000)
    println("donutStock Awake!!!")
    if(donut == "Vanilla Donut") Some(10) else None
  }

  def buyDonuts1(qty: Int): Future[Boolean] = Future{
    println(s"buying $qty donuts ... sleep for 3 seconds")
    Thread.sleep(3000)
    println("buyDonuts1 Awake!!!")
    if(qty > 0) true else false
  }

  def processPayment1(): Future[Unit] = Future {
    println("processPayment ... sleep for 1 second")
    Thread.sleep(1000)
    println("processPayment1 Awake!!!")
  }

  val futureOperations: List[Future[Any]] = List(donutStock1("Vanilla Donut"), buyDonuts1(10), processPayment1())

  val futureSequenceResults = Future.sequence(futureOperations)
  futureSequenceResults.onComplete{
    case Success(results) => println(s"Results : $results")
    case Failure(e) => println(e.getMessage)
  }
  (1 to 3).foreach(println(_))
  Thread.sleep(5000)
  println()
  //println(Await.result(futureSequenceResults, 5 seconds))

  /**
   * Traverse
   * the traverse function also allows you to fire a bunch of future operations in parallel and wait for their results.
   * The traverse function, though, has the added benefit of allowing you to apply a function over the future operations
   */
  val futureOperations1 = List(donutStock1("Vanilla Donut"), donutStock1("Plain Donut"))
  val futureTraverseResult = Future.traverse(futureOperations1){
    futureSomeQty => futureSomeQty.map(someQty => someQty.getOrElse(0))
  }

  futureTraverseResult.onComplete{
    case Success(res) => println(res)
    case Failure(e) => println(e.getMessage)
  }
  Thread.sleep(5000)
  println()

  /**
   * Future foldLeft
   * Scala provides a foldLeft function for futures and, as per the Scala API documentation,
   * the foldLeft on your future operations will be run asynchronously from left to right
   */
  val futureOperations2 = List(donutStock1("Vanilla Donut"), donutStock1("Plain Donut"))

  val futureFoldLeft = Future.foldLeft(futureOperations2)(0){
    case (a,b) => a + b.getOrElse(0)
  }

  futureFoldLeft.onComplete{
    case Success(res) => println(res)
    case Failure(e) => println(e.getMessage)
  }
  Thread.sleep(3000)
  println()
  //Similar for reduceLeft

  /**
   * Future firstCompletedOf
   * There may be times when you'd be looking to fire a bunch of futures and continue processing as soon as you've received the first result from any one of them.
   * This behaviour is perhaps observed in micro-services architecture for sharding traffic. With this in mind, Scala provides a handy Future.firstCompletedOf() function to achieve just that.
   * As per the Scala API documentation, firstCompletedOf() function, as its name implies, will return the first future that completes
   */
  def donutStock3(donut: String): Future[Option[Int]] = Future {
    println("checking donut stock.....sleep for 2 seconds")
    //Thread.sleep(2000)
    println("donutStock Awake!!!")
    if(donut == "Vanilla Donut") Some(10) else None
  }
  val futureOperations3 = List(donutStock3("Vanilla Donut"), donutStock3("Plain Donut"), donutStock3("Plain Donut"), donutStock3("Plain Donut"), donutStock3("Plain Donut"))
  val futureFirstCompletedResult = Future.firstCompletedOf(futureOperations3)
  futureFirstCompletedResult.onComplete{
    case Success(res) => println(res) //It can be None or Some(10)
  }
  println()

  /**
   * Future Zip
   */
  def donutPrice(): Future[Double] = Future.successful(3.25)
  val donutStockAndPrice = donutStock3("Vanilla Donut") zip donutPrice()
  donutStockAndPrice.onComplete{
    case Success(res) => println(res)
  }
  println()

  /**
   * Future andThen
   */
  val donutStockOperation = donutStock("Vanilla Donut")
  donutStockOperation.andThen{ case qty => println(qty)}
  println()

  /**
   * Future Configure ThreadPool
   * In comments Starting of object
   */
  def donutStock4(donut: String): Future[Int] = Future {
    println("checking donut stock")
    10
  }

  donutStock("Vanilla Donut").onComplete({
    case Success(res) => println(res)
  })
  println()

  /**
   * recover
   * recoverWith
   * fallbackTo
   */

  /**
   * Future Promise
   * So far we've been using future's return type to mark our methods to run asynchronously.
   * Scala provides an abstraction over future and it is called a Promise.
   * Promises can be useful when you want to capture the intent of your operation versus its behaviour.
   */
  def donutStock5(donut: String): Int = {
    if(donut == "Vanilla Donut") 10
    else throw new IllegalStateException("Out of Stock")
  }

  val donutStockPromise = Promise[Int]()
  val donutStockFuture = donutStockPromise.future
  donutStockFuture.onComplete{
    case Success(res) => println(res)
    case Failure(e) => println(e.getMessage)
  }

  val donut = "Vanilla Donut"
  //println(donut)
  if(donut == "Vanilla Donut")
    donutStockPromise.success(donutStock5(donut))
  else
    donutStockPromise.failure(Try(donutStock5(donut)).failed.get)
  Thread.sleep(2000)
  println()

  //Can also use Promise Complete method
  val donutStockPromise2 = Promise[Int]()
  donutStockPromise2.future.onComplete{
    case Success(res) => println(res)
    case Failure(e) => println(e.getMessage)
  }
  donutStockPromise2.complete(Try(donutStock5("qwerty")))

}
